function [ du, rhs_global, stress_Mises_array, dt, ...
    dv_tang_contact ] = findEquilibrium(Edof, ...
    Ex, Ey, mat_par, regul_variable, dt, u, du, n_el, n_dof, dof_free, ...
    contact_nodes_coords, contact_dofs, active_nodes, y_top_r, ...
    friction, max_iter, err_tol)
%Function to represent FE equilibrium iteration.
%   Written to work for Penalty method.
%
% Rostyslav Skrypnyk.

active_nodes_coords = contact_nodes_coords(active_nodes,:);
active_dofs = contact_dofs(active_nodes,:); % Choose DOFs of the active nodes.

stress_Mises_array = zeros(n_el,1); % To store von Mises stress in
% the elements.
dv_tang_contact = zeros(length(active_nodes),2); % Tangential velocities of contact nodes.

converge = false; % Assume it does not conferge.

for iter=1:max_iter
    u_el_array = extract(Edof,u);
    du_el_array = extract(Edof,du); % Extract nodal
    % increments of the displacement for each element.
    
    K_global = zeros(n_dof); % Global stiffness matrix.
    rhs_global = zeros(n_dof,1); % Global right hand side vector ==
    % internal forces - external forces.
    
    for el=1:n_el
        [K_el, f_el, stresses_el] = elementRoutine(Ex(el,:), Ey(el,:),...
            mat_par, u_el_array(el,:)', du_el_array(el,:)');
        
        % Assemble global stiffness matrix and RHS vector:
        K_global(Edof(el,2:end),Edof(el,2:end)) = ...
            K_global(Edof(el,2:end),Edof(el,2:end)) + K_el;
        rhs_global(Edof(el,2:end)) = ...
            rhs_global(Edof(el,2:end)) + f_el;
        
        % For plotting:
        % Compute von Mises stress in the element:
        stresses_dev_el = stresses_el;
        stresses_dev_el(1:3) = stresses_el(1:3) - ...
            ones(3,1)*sum(stresses_el(1:3))/3.0;
        stress_Mises = sqrt(1.5* (stresses_dev_el'*stresses_dev_el) );
        
        stress_Mises_array(el) = stress_Mises;
    end % Loop over elements.
    
    gap_func_array = computeGapFunc(active_nodes_coords(:,2), ...
        u(active_dofs(:,2))+du(active_dofs(:,2)), y_top_r);

    for node_n=1:length(gap_func_array)
        node_active_dofs = active_dofs(node_n,:);
        % Add contribution to the stiffness matrix and RHS vector from the
        % contact nodes:
        N_x = 0; % x component of the normal vector.
        N_y = 1; % y component of the normal vector.
        N_vector = [N_x,N_y];
            
        F_n = mat_par.penalty * gap_func_array(node_n);

        if friction
            % P = I - N (dyad) N:
            P = eye(length(N_vector)) - N_vector'*N_vector;
 
            % Tangential displacement velocity:
            dv_tang = P * du(node_active_dofs) / dt;

            % Regualarization function:
            regul_func = tanh(norm(dv_tang)/regul_variable);
   
            friction_force = - mat_par.friction * abs(F_n) * dv_tang ...
                / norm(dv_tang+eps) * regul_func;

            temp = dv_tang*dv_tang'/(dv_tang'*dv_tang+eps);
            
            dFrictionForce_dU = - mat_par.friction * ( ...
                sign(F_n-eps) * mat_par.penalty *...
                dv_tang / norm(dv_tang + eps) * N_vector * regul_func ...
                + abs(F_n+eps) / dt * ...
                (eye(length(N_vector)) - temp) * P / norm(dv_tang+eps) ...
                + abs(F_n+eps) / regul_variable / dt * (1 - regul_func^2) * ...
                temp * P );
        else
            friction_force = zeros(length(node_active_dofs),1);
            dFrictionForce_dU = zeros(length(node_active_dofs));
        end
        dGapFunc_dU = N_vector;

        K_global(node_active_dofs, node_active_dofs) = ...
            K_global(node_active_dofs, node_active_dofs) + ...
            mat_par.penalty*( dGapFunc_dU' * dGapFunc_dU ) - ...
            dFrictionForce_dU;
        
        rhs_global(node_active_dofs) = ...
            rhs_global(node_active_dofs) + N_vector' * F_n ...
            - friction_force;
    end % loop over active nodes.
    
    delta_du =  - K_global(dof_free,dof_free) \ ...
        rhs_global(dof_free);

    du(dof_free) = du(dof_free) + delta_du;

    if norm(rhs_global(dof_free)) < err_tol 
        converge = true;
        break
    end
end % Equilibrium iteration.

if converge
    % Report about completion:
    fprintf(['  Equilibrium was found in %d iterations with %d active ',...
        'nodes.\n'], iter, sum(active_nodes))
    
for n=1:length(contact_dofs(:,1))
    node_dofs = contact_dofs(n,:);
    dv_tang = P * du(node_dofs) / dt;
    dv_tang_contact(n,:) = dv_tang'; 
end
    
else
    error('Equilibrium was not found in %d iterations.\n', max_iter)
end

end % function findEquilibrium.

