function [ du, Lagrange, rhs_global_u, stress_Mises_array, ...
    dt_new, dv_tang_contact ] = findEquilibriumLagrange( Edof, Ex, Ey, ...
    mat_par, regul_variable, dt, u, du, Lagrange, n_el, n_dof_u, dof_free_u, ...
    contact_nodes_coords, contact_dofs, active_nodes, y_top_r, ...
    friction, max_iter, err_tol, gap_tol)
%Function to represent FE equilibrium iteration.
%   Written to work for Lagrange multipliers method.
%
% Rostyslav Skrypnyk.

active_nodes_coords = contact_nodes_coords(active_nodes,:);
active_dofs = contact_dofs(active_nodes,:); % Choose DOFs of the active nodes.

n_dof_free_u = length(dof_free_u);
n_extra_dof = sum(active_nodes); % Number of Lagrange multiplier DOFs.
n_total_dof = n_dof_u + n_extra_dof; % Total number of DOFs.
dof_extra = [n_dof_u+1:n_total_dof]';
dof_free_total = [dof_free_u; dof_extra];

stress_Mises_array = zeros(n_el,1); % To store von Mises stress in
% the elements.
dv_tang_contact = zeros(length(active_nodes),2); % Tangential velocities of contact nodes.

converge = false; % Assume it does not converge.

for iter=1:max_iter
    u_el_array = extract(Edof,u);
    du_el_array = extract(Edof,du); % Extract nodal
    % increments of the displacement for each element.
    
    K_global = zeros(n_total_dof); % Global stiffness matrix.
    rhs_global = zeros(n_total_dof,1); % Global right hand side vector ==
    % internal forces - external forces.
    
    for el=1:n_el
        [K_el,f_el, stresses_el] = elementRoutine(Ex(el,:), Ey(el,:),...
            mat_par, u_el_array(el,:)', du_el_array(el,:)');
        
        % Assemble global stiffness matrix and RHS vector:
        K_global(Edof(el,2:end),Edof(el,2:end)) = ...
            K_global(Edof(el,2:end),Edof(el,2:end)) + K_el;
        rhs_global(Edof(el,2:end)) = ...
            rhs_global(Edof(el,2:end)) + f_el;
        
        % For plotting:
        % Compute von Mises stress in the element:
        stresses_dev_el = stresses_el;
        stresses_dev_el(1:3) = stresses_el(1:3) - ...
            ones(3,1)*sum(stresses_el(1:3))/3.0;
        stress_Mises = sqrt(1.5* (stresses_dev_el'*stresses_dev_el) );
        
        stress_Mises_array(el) = stress_Mises;
    end % Loop over elements.
    
    gap_func_array = computeGapFunc(active_nodes_coords(:,2), ...
        u(active_dofs(:,2))+du(active_dofs(:,2)), y_top_r);

    l_i = 0;
    for node=find(active_nodes)' % transpose to iterate through the array.
        node_active_dofs = contact_dofs(node,:);
        l_i = l_i + 1;
        % Add contribution to the stiffness matrix and RHS vector from the
        % contact nodes:
        N_x = 0; % x component of the normal vector.
        N_y = 1; % y component of the normal vector.
        N_vector = [N_x,N_y];
     
        if friction
            % P = I - N (dyad) N:
            P = eye(length(N_vector)) - N_vector'*N_vector;

            % Tangential displacement velocity:
            dv_tang = P * du(node_active_dofs) / dt; 
            
            % Regualarization function:
if iter > 15
    scale_coef = 0.955;
else
    scale_coef = 1;
end
            regul_func = scale_coef * tanh(norm(dv_tang)/regul_variable);

            friction_force = - mat_par.friction * abs(Lagrange(l_i)) * ...
                             dv_tang / norm(dv_tang+eps) * regul_func;
            
            temp = dv_tang*dv_tang'/(dv_tang'*dv_tang+eps);
            
            dFrictionForce_dU = - mat_par.friction * abs(Lagrange(l_i)+eps) * ...
                                       ((eye(length(N_vector)) - temp) / norm(dv_tang+eps) * ...
                                       regul_func + (1 - regul_func^2) * temp / ...
                                       regul_variable) * P / dt;
     
            dFrictionForce_dLagrange = - mat_par.friction * dv_tang * sign(Lagrange(l_i)-eps) / ...
                                       (norm(dv_tang)+eps) * regul_func;
        else
            friction_force = zeros(length(node_active_dofs),1);
            dFrictionForce_dU = zeros(length(node_active_dofs));
            dFrictionForce_dLagrange = zeros(length(node_active_dofs),1);
        end % friction part.
        dGapFunc_dU = N_vector;

        K_global(node_active_dofs,node_active_dofs) = ...
            K_global(node_active_dofs,node_active_dofs) - dFrictionForce_dU;
        
        K_global(node_active_dofs, dof_extra(l_i)) = dGapFunc_dU' - ...
            dFrictionForce_dLagrange;
        
        K_global(dof_extra(l_i), node_active_dofs) = dGapFunc_dU;
        
        rhs_global(node_active_dofs) = rhs_global(node_active_dofs) + ...
            dGapFunc_dU' * Lagrange(l_i) - friction_force;
    end % loop over active nodes.
    rhs_global(dof_extra) = gap_func_array;

    delta_dUnknowns =  - K_global(dof_free_total,dof_free_total) \ ...
        rhs_global(dof_free_total);
    
    du(dof_free_u) = du(dof_free_u) + delta_dUnknowns(1:n_dof_free_u);
    Lagrange = Lagrange + delta_dUnknowns(end-n_extra_dof+1:end);

    if norm(rhs_global(dof_free_u)) < err_tol && ...
       norm(rhs_global(dof_extra)) < gap_tol
        converge = true;
        break
    end
end % Equilibrium iteration.

rhs_global_u = rhs_global(1:n_dof_u);

if converge
    dt_new = dt;
    % Report about completion:
    fprintf(['  Equilibrium was found in %d iterations with %d active ',...
        'nodes.\n'], iter, sum(active_nodes))

for n=1:length(contact_dofs(:,1))
    node_dofs = contact_dofs(n,:);
    dv_tang = P * du(node_dofs) / dt;
    dv_tang_contact(n,:) = dv_tang'; 
end

else
    if dt < 1e-5
       error('Equilibrium was not found in %d iterations with dt=%f.\n', [max_iter, dt]) 
    else
        dt_new = 0.5 * dt;
        fprintf('  Equilibrium was not found in %d iterations. Reducing dt by 50%%.\n', max_iter)
    end
end

end % function findEquilibriumLagrange.

