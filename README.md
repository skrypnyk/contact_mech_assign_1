[//]: # (To preview markdown file in Emacs type C-c C-c p)

# Computer assignment 1: One body contact
MATLAB code to solve contact between an elastic body and a rigid plane under
plane strain conditions with friction.

## Task A
Penalty method.

## Task B
Lagrange multiplier method.

## Task C
Penalty or Lagrange method with friction.

