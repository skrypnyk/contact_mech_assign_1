% A script for the assignment 1 on the computational part of Contact
% Mechanics course.
%% Assignment 1: One body.
% Based on CALFEM, solve contact problem between elastic body and rigid 
%plane under plane strain conditions with friction.
%
% Rostyslav Skrypnyk. Mar 9, 2016.

%% Task (a).
% Assume the rail is rigid while the wheel is elastic.
% Increase the verical displacement u_vert of the upper boundary of the 
% piece of the wheel while fixing the corresponding horizontal displacement
% u_hor = 0. Compute the total vertical reaction force F_vert vs the 
% displacement u_vert. Also compute the field of von Mises effective stress
% at a proper choice of u_vert. Use the penalty method. Show the dependence
% on the penalty parameter. Assume no friction.
%
% Instructions: 1. Set 'penalty_method = true';
%               2. Set 'friction = false'.

%% Task (b).
% Same as (a), but use the Lagrange multipliers method.
% 
% Instructions: 1. Set 'penalty_method = false';
%               2. Set 'friction = false'.

%% Task (c).
% Include friction modeling combined with the Penalty or the Lagrange
% multipliers method. First displace the upper part of the wheel downwards
% to u_vert = 0.15 mm, then displace the upper part of the wheel
% horizontally to u_hor = 0.05 mm. Assume the time 0.05 s for each of
% these displacements. Assume regularized Coulomb's law. Plot the
% tangential velocities at the contact surface e.g. identify stick and slip
% regions.
%
% Instructions: 1.  Set 'friction = true'.

%% Set-up
close all
clear variables
format compact
clc

addpath([pwd , '/meshes'])
addpath(genpath('~/Documents/MATLAB/calfem-3/')) % Add Calfem routines.

% Analysis settings:
mesh_coarse = false; % Choose coarse or dense mesh.
penalty_method = false; % Choose between penalty and Lagrange multipliers 
% method.
friction = true; % Choose between frictionless contact or with friction.

% Spatial discretization:
if mesh_coarse
    mesh_r = load('calfem_rail_coarse.mat');
    mesh_w = load('calfem_wheel_coarse.mat'); % 1x1 structure array
    % with the following fields (with description from CALFEM manual):
    % Ex, Ey -- arrays of rows [x_1 x_2 ... x_nen] and [y_1 y_2 ... y_nen], 
    %           where nen = number of element nodes and row i gives x  
    %           (or y) coordinates of the element defined in row i of Edof. 
    % Coord -- global nodal coordinate matrix; array of [x y] rows.
    % Edof -- element topology matrix; array of [el dof_1 dof_2 ... dof_ned] 
    %         rows; el = element number, ned = number of element dof.
    % Dof -- global topology matrix; array of [k l .. m] rows. The nodal 
    %        coordinates defined in row i of Coord correspond to the 
    %        degrees  of freedom of row i in Dof.
    % dof_free
    % dof_prescr
    % contact_nodes.
    fprintf('Coarse mesh was chosen.\n\n');
else
    mesh_r = load('calfem_rail_dense.mat');
    mesh_w = load('calfem_wheel_dense.mat');
    
    fprintf('Dense mesh was chosen.\n\n');
end

if penalty_method
    fprintf('Penalty method was chosen.\n\n');
else
    fprintf('Lagrange multipliers method was chosen.\n\n');
end

if friction
    fprintf('Contact with friction.\n\n');
else
    fprintf('Frictionless contact.\n\n');
end

% Loading:
u_vert = - 0.15 % Vertical displacement of the top of the wheel [mm].
u_horiz = 0.05

% Time stepping:
n_steps_vert = 100;
n_steps_horiz = 50;
n_steps = n_steps_vert + n_steps_horiz;
t_end_vert = 0.05; % [s].
t_end_horiz = 0.05;
t_end_total = t_end_vert + t_end_horiz;
dt_initial = t_end_vert/n_steps_vert;

% Material parameters:
mat_par.E = 200.e3; % Young's modulus [MPa=N/mm^2].
mat_par.nu = 0.3; % Poisson's ratio [unitless].
mat_par.penalty = 1.e7; % Penalty parameter [N/mm].
mat_par.friction = 0.3; % Friction coefficient [unitless].

eps_0 = 1.e-2; % Regularization variable [mm/s].

% Tolerances:
err_tol = 1.e-7; % Error tolerance for the Newton's iteration.
gap_tol = 1.e-20 % Tolerance for the getActiveNodes function.
max_iter = 50;

% Other quantities:
n_dof_w = max(max(mesh_w.Dof)); % Number of degrees of freedom.
n_dof_el_w = size(mesh_w.Edof,2) - 1; % Number of DOFs per element.
n_el_w = size(mesh_w.Edof,1); % Number of elements.

dof_free_w = mesh_w.dof_free;

dof_prescr_horiz_w = mesh_w.dof_prescr(1:2:end);
dof_prescr_vert_w = mesh_w.dof_prescr(2:2:end);
y_top_r = max(mesh_r.Coord(:,2)); % Max ordinate in the rail geometry.
contact_dofs_w = mesh_w.Dof(mesh_w.contact_nodes,:); % DOFs of the nodes on 
% the contact surface.
contact_nodes_coords_w = mesh_w.Coord(mesh_w.contact_nodes,:);
dt_old = dt_initial; % Start with the initial time increment even if it has been modified.
dt_scale = 1;

% Initialize:
dof_prescr_w = dof_prescr_vert_w;
u_w = zeros(n_dof_w,1); % Displacements of the wheel at time t+dt.

active_init = getActiveNodes(computeGapFunc(contact_nodes_coords_w(:,2), ...
                                            u_w(contact_dofs_w(:,2)), ...
                                            y_top_r), ...
                             gap_tol);
Lagrange_w = zeros(sum(active_init), 1); % Check how many nodes are in the immediate contact and initialize with zero.
u_hist_w = [];
forces_hist_w = [];
stress_Mises_hist_w = [];

%% Solution
time = dt_initial;
du_w = zeros(n_dof_w,1);
while time <= t_end_total
    if time <= t_end_vert 
        du_prescr_w = dt_scale * u_vert/n_steps_vert*ones(size(dof_prescr_w)); % Increment of the 
        % prescribed vertical displacement.
    
        % Compute gap function for the whole contact node set, assuming rigid body motion:
        gap_func_array_w = computeGapFunc(contact_nodes_coords_w(:,2), ...
                            u_w(contact_dofs_w(:,2)) + du_w(contact_dofs_w(:,2)), ...
                            y_top_r);
        % Find which nodes from the contact set are active:
        active_nodes_w = getActiveNodes(gap_func_array_w, gap_tol);
    else % Prescribe horizontal DOFs:
        fprintf('Sliding.\n\n');

        dof_prescr_w = dof_prescr_horiz_w;
        du_prescr_w = dt_scale * u_horiz/n_steps_horiz*ones(size(dof_prescr_w));
        dt_old = dt_scale * t_end_horiz/n_steps_horiz;
        
        % Fix vertical position:
        du_w(dof_prescr_vert_w) = zeros(size(dof_prescr_vert_w));
    end
    
    % Prescribe BCs:
    du_w(dof_prescr_w) = du_prescr_w;
    
    for iter=1:max_iter % Contact iteration:
        fprintf(' Start contact iteration %d.\n', iter)
        if penalty_method
            % Solve for equilibrium and update increment of the displacements:
            [du_w, forces_w, stress_Mises_array_w, dt_new, ...
             dv_tang_contact_w] = findEquilibrium(...
                mesh_w.Edof, mesh_w.Ex, mesh_w.Ey, ...
                mat_par, eps_0, dt_old, u_w, du_w, n_el_w, n_dof_w, dof_free_w, ...
                contact_nodes_coords_w, contact_dofs_w, active_nodes_w, y_top_r, ...
                friction, max_iter, err_tol);
            
            % Recompute gap function:
            gap_func_array_w = computeGapFunc(contact_nodes_coords_w(:,2), ...
                u_w(contact_dofs_w(:,2))+du_w(contact_dofs_w(:,2)), y_top_r);
            
            % Find the active nodes after the equilibrium has been found:
            active_nodes_w_new = getActiveNodes(gap_func_array_w, gap_tol);
            
        else % Lagrange multiplier method:
            active_sum = sum(active_nodes_w);
            if active_sum ~= length(Lagrange_w)
                L_w_temp = Lagrange_w(Lagrange_w < 0); % Positive values mean nodes are not in contact.
                Lagrange_w = zeros(active_sum,1);
                active_ind_old_w = find(active_nodes_w_new); % Indices of active nodes from end of previous iteration.
                active_ind_w = find(active_nodes_w);
                [~,~,active_ind_both_w] = intersect(active_ind_old_w, active_ind_w);
                Lagrange_w(active_ind_both_w) = L_w_temp;
            end
            % Solve for equilibrium and update increment of the displacements:
            [du_w, Lagrange_w, forces_w, stress_Mises_array_w , dt_new, ...
             dv_tang_contact_w] = findEquilibriumLagrange(...
                mesh_w.Edof, mesh_w.Ex, mesh_w.Ey, ...
                mat_par, eps_0, dt_old, u_w, du_w, Lagrange_w, n_el_w, n_dof_w, dof_free_w, ...
                contact_nodes_coords_w, contact_dofs_w, active_nodes_w, y_top_r, ...
                friction, max_iter, err_tol, gap_tol);
            if dt_new < dt_old
                break % If dt decreased, quit and ...
            end
            % Recompute gap function:
            gap_func_array_w = computeGapFunc(contact_nodes_coords_w(:,2), ...
                u_w(contact_dofs_w(:,2))+du_w(contact_dofs_w(:,2)), y_top_r);

            % Find the active nodes after the equilibrium has been found:
            active_nodes_w_new = getActiveNodesLagrange(gap_func_array_w, gap_tol, Lagrange_w);
        end
        
        if all(active_nodes_w==active_nodes_w_new)
            % If the set of active contact nodes has not changed:
            break
        end
        active_nodes_w = active_nodes_w_new;
    end % Contact iteration.
    
    if dt_new < dt_old
        dt_old = dt_new; % ... retry.
        du_w = dt_new / dt_old * du_w;
        continue
    end
    
    if iter==max_iter
        error('Contact iteration did not converge in %d iterations.\n', ...
            max_iter)
    else
        fprintf('Competed %.1f %%.\n\n', time / t_end_total * 100 )
    end
    dt_scale = dt_new / dt_old;
    u_w = u_w + du_w;
    u_hist_w = [u_hist_w, u_w];
    forces_hist_w = [forces_hist_w, forces_w];
    stress_Mises_hist_w = [stress_Mises_hist_w, stress_Mises_array_w];
    time = time + dt_new;
end % Loop over time.

%% Post-process
% Save
% max_penetration = abs(min(gap_func_array_w(gap_func_array_w<0)));
% save('penalty1e7.mat','u_hist_w','dof_prescr_vert_w','u_vert', ...
%      'forces_hist_w','max_penetration');
% save('lagrange.mat','u_hist_w','dof_prescr_vert_w','u_vert', ...
%      'forces_hist_w');

% Plots:
figure(1)
% Plot undeformed shape:
eldraw2(mesh_w.Ex, mesh_w.Ey,[2 1 0]) % Black.
%eldraw2(mesh_r.Ex, mesh_r.Ey,[1 4 0]) % Red.

% Plot deformed shape:
u_el_array = extract(mesh_w.Edof, u_w);
eldisp2(mesh_w.Ex,mesh_w.Ey,u_el_array,[1,2,0],1);
xlim([-36,36])
ylim([0,41])

% Task a:
% plotstask1

% Task b:
% plotstask2

% Task c:
plotstask3