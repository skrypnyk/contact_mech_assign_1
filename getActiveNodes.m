function [ active_nodes ] = getActiveNodes( gap_func_array, tol )
%Returns logical array of nodes in contact.
% 
% Rostyslav Skrypnyk.

active_nodes = logical(zeros(size(gap_func_array)));

active_nodes(gap_func_array < tol) = true; % Mark nodes that are in contact.

end

