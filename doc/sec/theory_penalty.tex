\section{Theory}
\label{sec:theory}

\subsection{Penalty method}
\label{sec:penalty-method}

The Penalty method introduces a spring at the contact nodes of stiffness
\(\epsilon_{N}\). This stiffness is referred to as \textit{penalty parameter}.
The advantage of this method is that the set of unknowns remains unchanged.
However, the correct solution is achieved when
\( \epsilon_{N} \rightarrow \infty \), which leads to an ill-conditioned matrix.
Therefore, the penalty parameter must be limited, which results in (small)
penetration.

\subsubsection{Frictionless contact}
\label{sec:penalty-frictionless}

Application of principle of virtual work to quasi-static equilibrium
equations gives
\begin{equation}
  \label{eq:penalty-virtual-work}
  \underbrace{ \delta \ubarbold{u}_{\text{F}}^{T}
    \left( \ubarbold{f}_{\text{int,F}} - \ubarbold{f}_{\text{ext,F}} \right)
  }_{\delta \Pi} +
  \epsilon_{N}  \sum_{i=1}^{n_{\text{c}}} g(\bm{x}_{s_{i}}) \delta g(\bm{x}_{s_{i}}) = 0
\end{equation}
with
\begin{equation*}
  \delta g(\bm{x}_{s_{i}}) = \frac{\partial g}{\partial \bm{x}_{s_{i}}}
  \delta \bm{x}_{s_{i}} =  \frac{\partial g}{\partial \bm{u}_{s_{i}}}
  \delta \bm{u}_{s_{i}} 
\end{equation*}
where \(\bm{x}_{s_{i}}\) is a current position of contact node \(s_{i}\),
\(\ubarbold{f}_{\text{int,F}} \) and \(\ubarbold{f}_{\text{ext,F}}\)
are internal and external force vectors, respectively.
To satisfy equation \eqref{eq:penalty-virtual-work}, the following must
hold:
\begin{empheq}[left={\empheqlbrace}]{align} 
  \ubarbold{G}_{\text{F}} = \ubarbold{f}_{\text{int,F}} - \ubarbold{f}_{\text{ext,F}}
  &= \ubarbold{0}, & \text{F} \notin \{ s_{1}, \dots, s_{n_{\text{c}}}\}
  \label{eq:penalty-expanded-1} \\
  \ubarbold{G}_{\text{F}} + \epsilon_{N} g (\ubarbold{u}_{\text{F}})
  \frac{\partial g  (\ubarbold{u}_{\text{F}}) }{\partial \ubarbold{u}_{\text{F}}}
  &= \ubarbold{0}, & \text{F} \in \{ s_{1}, \dots, s_{n_{\text{c}}}\}
  \label{eq:penalty-expanded-2}
\end{empheq}
The term \(\epsilon_{N} g (\ubarbold{u}_{\text{F}})
\dfrac{\partial g  (\ubarbold{u}_{\text{F}}) }{\partial \ubarbold{u}_{\text{F}}}\)
in equation \eqref{eq:penalty-expanded-2} is an external force and must come
with a minus sign, which it does. The minus sign is hidden in the definition of
the gap function \(g (\ubarbold{u}_{\text{F}}) \) that takes negative values
for overlapped surfaces.

If we define \( \ubar{G}_{\text{c}} \) and \( \uubarbold{C}_{\text{c}} \) as
\begin{equation*}
  \ubar{G}_{\text{c}} =
  \begin{bmatrix}
    g(\bm{u}_{s_{1}}) \\ \vdots \\ g(\bm{u}_{s_{n_{\text{c}}}})
  \end{bmatrix},
  \quad
  \uubarbold{C}_{\text{c}} =
  \frac{\partial \ubar{G}_{\text{c}}}{\partial \ubarbold{u}_{\text{F}}}
  \begin{bmatrix}
    \partial g(\bm{u}_{s_{1}}) / \partial \ubarbold{u}_{\text{F}} \\
    \vdots \\
    \partial g(\bm{u}_{s_{n_{\text{c}}}}) / \partial \ubarbold{u}_{\text{F}}
  \end{bmatrix}
\end{equation*}
then the equations \eqref{eq:penalty-expanded-1}-\eqref{eq:penalty-expanded-2} can be rewritten more
compactly (subscript P for penalty):
\begin{equation}
  \label{eq:penalty-compact}
  \ubarbold{G}_{\text{P}} (\ubarbold{u}_{\text{F}}) = \ubarbold{G}_{\text{F}} + \epsilon_{N}
  \uubarbold{C}_{\text{c}}^{T} \ubar{G}_{\text{c}} = \ubarbold{0}
\end{equation}
The equation \eqref{eq:penalty-compact} can be solved by means of Newton's
method:
\begin{align}
  \ubarbold{G}(\ubarbold{u}_{k+1}) &= \ubarbold{G}(\ubarbold{u}_{k}) +
                                     \left( \frac{d \ubarbold{G}}{d \ubarbold{u}} \right)_{k}
                                     \Delta \ubarbold{u} = \ubarbold{0} \label{eq:newton-method} \\
  \ubarbold{u}_{k+1} &= \ubarbold{u}_{k} + \Delta \ubarbold{u} \label{eq:newton-update}
\end{align}
where subscript \(k\) is an iteration number (other subscripts are omitted for
clarity). The tangent stiffness takes the following form:
\begin{equation}
  \label{eq:penalty-stiffness}
  \frac{d \ubarbold{G}}{d \ubarbold{u}} =
  \frac{d \ubarbold{G}_{\text{F}}}{d \ubarbold{u}_{\text{F}}} +
  \epsilon_{N}
  \frac{ d \left( \ubarbold{C}_{\text{c}}^{T} \ubar{G}_{\text{c}} \right) }
  { d \ubarbold{u}_{\text{F}} }
\end{equation}
In the case of linear constraints, i.e.\ when \( d g / d \bm{u} \)
is constant (one of the contact surfaces is a plane), the tangent stiffness
can be simplified to
\begin{equation}
  \label{eq:penalty-stiffness-linear}
  \frac{ d \ubarbold{G}}{ d \ubarbold{u}} =
  \frac{ d \ubarbold{G}_{\text{F}}}{ d \ubarbold{u}_{\text{F}}} +
  \epsilon_{N} \ubarbold{C}_{\text{c}}^{T} \ubarbold{C}_{\text{c}}
\end{equation}

\subsubsection{Frictional contact}
\label{sec:penalty-frictional}

Taking friction into account modifies the equilibrium equations for the nodes in
contact \( \text{F} \in \{ s_{1}, \dots, s_{n_{\text{c}}} \} \):
\begin{equation}
  \label{eq:penalty-compact-friction}
  \ubarbold{G}_{\text{F}} + \epsilon_{N} \uubarbold{C}_{\text{cF}}^{T} \ubar{G}_{\text{cF}} -
  \ubarbold{t}_{t} = \ubarbold{0}
\end{equation}
with frictional force vector \(\ubarbold{t}_{t}\), whose every element is defined here as
\begin{equation}
  \label{eq:penalty-friction-force}
  \bm{t}_{t} = - \mu \left\vert P_{N} \right\vert
  \frac{ \dot{\bm{u}}_{\text{F}t} }{ \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert }
  \varphi \left( \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert \right)
\end{equation}
where \( P_{N}\) is a normal component of the contact force:
\begin{equation}
  \label{eq:penalty-contact-force}
  P_{N} = \epsilon_{N} g \left( \bm{u}_{\text{F}} \right)
\end{equation}
\( \dot{\bm{u}}_{\text{F}t} \) is a tangential velocity that
can be written as
\begin{equation}
  \label{eq:tang-velocity}
  \dot{\bm{u}}_{\text{F}t} = \left( \bm{I} - \bm{N} \otimes \bm{N} \right)
  \cdot \dot{\bm{u}}_{\text{F}}
\end{equation}
with normal vector \(\bm{N}\). It is approximated using backward Euler:
\begin{equation}
  \label{eq:backward-euler}
  \dot{\bm{u}} \approx \frac{\bm{u}_{k+1} - \bm{u}_{k}}{dt}
\end{equation}
\( \varphi \left( \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert \right) \) is
the following regularisation function of the Coulomb's law:
\begin{equation}
  \label{eq:regularisation-function}
  \varphi \left( \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert \right) =
  \tanh \left( \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert / \epsilon_{0} \right)
\end{equation}
The frictional force \(\bm{t}_{t}\) comes with a minus sign in
equation \eqref{eq:penalty-compact-friction} because it is an external
force. Furthermore, in equation \eqref{eq:penalty-friction-force},
it is defined with a minus sign because it acts in the direction,
opposite to the direction of motion.

The tangent stiffness in the case of linear constraints is the following
(omitting the underline for simplicity but keeping in mind that the norms
are taken on the node basis, rather than the entire set):
\begin{equation}
  \label{eq:penalty-stiffness-linear-friction}
  \frac{ d \bm{G}}{ d \bm{u}} =
  \frac{ d \bm{G}_{\text{F}}}{ d \bm{u}_{\text{F}}} +
  \epsilon_{N} \bm{C}_{\text{cF}}^{T} \bm{C}_{\text{cF}} -
  \frac{ d \bm{t}_{t}}{ d \bm{u}_{\text{F}} }
\end{equation}
with
\begin{align}
  % dG / du:
  \frac{ d \bm{G}_{\text{F}}}{ d \bm{u}_{\text{F}}} 
  &= \bm{K}_{\text{FF}} \\
  % dt / du:
  \frac{ d \bm{t}_{t}}{ d \bm{u}_{\text{F}}}
  &= - \mu \left[ 
    \frac{ P_{N} }{ \left\vert P_{N} \right\vert }
    \epsilon_{N} \bm{N}
    \frac{ \dot{\bm{u}}_{\text{F}t} }{ \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert }
    \varphi \left( \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert \right) \right. \nonumber \\ 
  &\phantom{= - \mu \left[ \right.}
    + \frac{\left\vert P_{N} \right\vert}{dt} 
    \left(
    \frac{\bm{I}}{ \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert }
    - \frac{ \dot{\bm{u}}_{\text{F}t} \otimes \dot{\bm{u}}_{\text{F}t} }
    { \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert^{3} }
    \right)
    \left( \bm{I} - \bm{N} \otimes \bm{N} \right) \nonumber \\
  &\phantom{= - \mu \left[ \right.} \left.
    + \frac{\left\vert P_{N} \right\vert}{\epsilon_{0} dt} 
    \left[
    1 - \varphi^{2} \left( \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert \right)
    \right]
    \frac{ \dot{\bm{u}}_{\text{F}t} \otimes \dot{\bm{u}}_{\text{F}t} }
    { \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert^{2} }
    \left( \bm{I} - \bm{N} \otimes \bm{N} \right)
    \right]
\end{align}
The derivation of the expressions are given in appendix \ref{app:derivatives}.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
