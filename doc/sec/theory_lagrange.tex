\subsection{Lagrange method}
\label{sec:lagrange-method}

This method relies on the Kuhn-Tucker conditions:
\begin{equation}
  \label{eq:kuhn-tucker}
  g \geq 0, \quad \lambda \leq 0, \quad \lambda g = 0
\end{equation}
i.e.\ the gap function \(g(u)\) is zero at the point of contact
and the additional variable \(\lambda\) called
\textit{Lagrange multiplier}, that has unit of force, is negative
in this case.
The method introduces additional degrees of freedom (DOFs) to the
system with corresponding equations of the form
\begin{equation}
  \label{eq:additional-eq-lagrange}
  g(u) = g_{0} + u_{i} - u_{j} = 0
\end{equation}
where \(g_{0}\) is an initial gap between a pair of nodes with DOFs \(i\) and \(j\).
The advantage of this method is its precision, since no
penetration is allowed. However, the additional DOFs \(\lambda\)
(that represent exact forces at the points of contact) increase the
size of the system of equations and, hence, increase the computational
cost.

\subsubsection{Frictionless contact}
\label{sec:lagrange-frictionless}

From the principle of virtual work:
\begin{equation}
  \label{eq:lagrange-virtual-work}
  \underbrace{ \delta \ubarbold{u}_{\text{F}}^{T}
    \left( \ubarbold{f}_{\text{int,F}} - \ubarbold{f}_{\text{ext,F}} \right)
  }_{\delta \Pi} +
  \sum_{i=1}^{n_{\text{c}}} \left[ \delta \lambda_{s_{i}} g (\bm{x}_{s_{i}}) +
  \lambda_{s_{i}} \delta g (\bm{x}_{s_{i}}) \right] = 0
\end{equation}
To satisfy this equation, the following must hold:
\begin{empheq}[left={\empheqlbrace}]{align} 
  \ubarbold{G}_{\text{F}} = \ubarbold{f}_{\text{int,F}} - \ubarbold{f}_{\text{ext,F}}
  &= \ubarbold{0},& & \text{F} \notin \{ s_{1}, \dots, s_{n_{\text{c}}}\}
  \label{eq:lagrange-expanded-1} \\
  \ubarbold{G}_{\text{F}} + \lambda_{\text{F}}
  \frac{\partial g  (\ubarbold{u}_{\text{F}}) }{\partial \ubarbold{u}_{\text{F}}}
  &= \ubarbold{0},& & \text{F} \in \{ s_{1}, \dots, s_{n_{\text{c}}}\}
  \label{eq:lagrange-expanded-2} \\
  g (\ubarbold{u}_{\text{F}}) &=0,& & \text{F} \in \{ s_{1}, \dots, s_{n_{\text{c}}}\}
  \label{eq:lagrange-expanded-3}
\end{empheq}
The system can be rewritten more compactly by introducing
\( \ubar{\Lambda} = \left[ \lambda_{s_{1}} \dots
  \lambda_{s_{n_{\text{c}}}} \right]^{T} \):
\begin{empheq}[left={\empheqlbrace}]{align} 
  \ubarbold{G}_{\text{F}} + \uubarbold{C}_{\text{c}}^{T} \ubar{\Lambda}
  &= \ubarbold{0}
  \label{eq:lagrange-compact-1} \\
  \ubar{G}_{\text{c}} &= \ubar{0}
  \label{eq:lagrange-compact-2}
\end{empheq}
and \( \ubarbold{w} = \left[ \ubarbold{u}_{\text{F}} ~\ubar{\Lambda} \right]^{T} \)
that yields (LM for Lagrange multiplier)
\begin{equation}
  \label{eq:lagrange-compact-single}
  \ubarbold{G}_{\text{LM}} \left( \ubarbold{w} \right) =
  \begin{bmatrix}
    \ubarbold{G}_{\text{F}} + \uubarbold{C}_{\text{c}}^{T} \ubar{\Lambda}
    \\
    \ubar{G}_{\text{c}}
  \end{bmatrix}
  = \ubarbold{0}
\end{equation}
Newton's method, summarised by equations
\eqref{eq:newton-method}-\eqref{eq:newton-update}, can be employed to
solve this system. The tangent stiffness reads
\begin{equation}
  \label{eq:lagrange-stiffness}
  \frac{ d \ubarbold{G}_{\text{LM}} }{ d \ubarbold{w} } =
  \begin{bmatrix}
    \dfrac{ d \ubarbold{G}_{\text{F}} }{ d \ubarbold{u}_{\text{F}} } +
    \dfrac{ d \left( \uubarbold{C}_{\text{c}}^{T} \ubar{\Lambda} \right) }
    {d \ubarbold{u}_{\text{F}} }
    & \uubarbold{C}_{\text{c}}^{T} \\
    \uubarbold{C}_{\text{c}}
    & \uubar{0} 
  \end{bmatrix}
\end{equation}
Special case of linear constraints:
\begin{equation}
  \label{eq:lagrange-stiffness-linear}
  \frac{ d \ubarbold{G}_{\text{LM}} }{ d \ubarbold{w} } =
  \begin{bmatrix}
    \dfrac{ d \ubarbold{G}_{\text{F}} }{ d \ubarbold{u}_{\text{F}} }
    & \uubarbold{C}_{\text{c}}^{T} \\
    \uubarbold{C}_{\text{c}}
    & \uubar{0} 
  \end{bmatrix}
\end{equation}

\subsubsection{Frictional contact}
\label{sec:lagrange-frictional}

Let the discretised gap function be of the following form:
\begin{equation}
  \label{eq:gap-function-fem}
  g \left( \bm{u}_{s_{i}} \right) = g_{0} + \bm{N}_{s_{i}} \cdot \bm{u}_{s_{i}}
\end{equation}
Then the equilibrium equations for each node in
contact \( \text{F} \in \{ s_{1}, \dots, s_{n_{\text{c}}} \} \)
for the Lagrange method including friction (dropping the underline since it
is node-based formulation) read
\begin{equation}
  \label{eq:lagrange-compact-friction}
  \bm{G}_{\text{F}} + \lambda_{\text{F}} \frac{\partial g}{\partial \bm{u}_{\text{F}}}
  - \bm{t}_{t}^{\text{LM}} =
  \bm{G}_{\text{F}} + \lambda_{\text{F}} \bm{N} - \bm{t}_{t}^{\text{LM}}
  = \bm{0}
\end{equation}
with frictional force defined as
\begin{equation}
  \label{eq:lagrange-friction-force}
   \bm{t}_{t}^{\text{LM}} = - \mu \left\vert \lambda_{\text{F}} \right\vert
  \frac{ \dot{\bm{u}}_{\text{F}t} }{ \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert }
  \varphi \left( \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert \right)
\end{equation}
and the rest of the entities were described in section \ref{sec:penalty-frictional}.

The tangent stiffness in the case of linear constraints thus reads 
\begin{equation}
  \label{eq:lagrange-stiffness-linear-friction}
  \frac{ d \ubarbold{G}_{\text{LM}} }{ d \ubarbold{w} } =
  \begin{bmatrix}
    \dfrac{ d \ubarbold{G}_{\text{F}} }{ d \ubarbold{u}_{\text{F}} } - 
    \dfrac{ d \ubarbold{t}_{t}^{\text{LM}} }{ d \ubarbold{u}_{\text{F}} } 
    & \uubarbold{C}_{\text{c}}^{T} -
    \dfrac{ d \ubarbold{t}_{t}^{\text{LM}} }{ d \ubar{\Lambda} } \\
    \uubarbold{C}_{\text{c}}
    & \uubar{0} 
  \end{bmatrix}
\end{equation}
with the following entities (omitting the underline for simplicity but keeping
in mind that the norms are taken on the node basis, rather than the entire set):
\begin{align}
  % dt / du:
  \frac{ d \bm{t}_{t}^{\text{LM}}}{ d \bm{u}_{\text{F}}}
  &= - \mu \left\vert \lambda_{\text{F}} \right\vert % it is \lambda and not \Lambda because the norm is node-based,
  % rather than entire set-based.
    \left[ \left( \bm{I} - \frac{ \dot{\bm{u}}_{\text{F}t} \otimes \dot{\bm{u}}_{\text{F}t} }
    { \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert^{2} } \right)
    \frac{ \varphi \left[ \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert \right] }
    { \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert } 
    +
    \frac{ \dot{\bm{u}}_{\text{F}t} \otimes \dot{\bm{u}}_{\text{F}t} }
    { \epsilon_{0} \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert^{2} }
    \left( 1 - \varphi^{2} \left[ \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert \right] \right)
    \right] \nonumber \\
  &\phantom{=}
    \cdot \left( \bm{I} - \bm{N} \otimes \bm{N} \right) / dt \\
    % dt / d(lambda):
  \frac{d \bm{t}_{t}^{\text{LM}}}{ d \lambda_{\text{F}}}
  &= - \mu \frac{ \lambda_{\text{F}} }{ \left\vert \lambda_{\text{F}} \right\vert }
    \cdot \frac{ \dot{\bm{u}}_{\text{F}t} }{ \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert }
    \varphi \left( \left\Vert \dot{\bm{u}}_{\text{F}t} \right\Vert \right)
\end{align}
The derivation of the expressions are given in appendix \ref{app:derivatives}.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
