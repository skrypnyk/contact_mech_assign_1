function [ du, Lagrange, rhs_global_u, stress_Mises_array ] = findEquilibriumLagrange( Edof, Ex, Ey, ...
    mat_par, u, du, du_prescr, n_el, n_dof_u, dof_free_u, ...
    dof_prescr_vert, contact_nodes_coords, contact_dofs, active_nodes, y_top_r, max_iter, err_tol)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

active_nodes_coords = contact_nodes_coords(active_nodes,:);
active_dofs = contact_dofs(active_nodes,:); % Choose DOFs of the active nodes.
active_dofs_reshaped = reshape(active_dofs',1,[]); % Reshape into a
% column vector for the RHS vector.

n_dof_free_u = length(dof_free_u);
n_extra_dof = sum(active_nodes); % Number of Lagrange multiplier DOFs.
n_total_dof = n_dof_u + n_extra_dof; % Total number of DOFs.
dof_extra = [n_dof_u+1:n_total_dof]';
dof_free_total = [dof_free_u; dof_extra];

stress_Mises_array = zeros(n_el,1); % To store von Mises stress in
% the elements.

% Prescribe BCs:
du(dof_prescr_vert) = du_prescr;

Lagrange = zeros(n_extra_dof,1);

converge = false; % Assume it does not conferge.

for iter=1:max_iter
    u_el_array = extract(Edof,u);
    du_el_array = extract(Edof,du); % Extract nodal
    % increments of the displacement for each element.
    
    K_global = zeros(n_total_dof); % Global stiffness matrix.
    rhs_global = zeros(n_total_dof,1); % Global right hand side vector ==
    % internal forces - external forces.
    
    for el=1:n_el
        [K_el,f_el, stresses_el] = elementRoutine(Ex(el,:), Ey(el,:),...
            mat_par, u_el_array(el,:)', du_el_array(el,:)');
        
        % Assemble global stiffness matrix and RHS vector:
        K_global(Edof(el,2:end),Edof(el,2:end)) = ...
            K_global(Edof(el,2:end),Edof(el,2:end)) + K_el;
        rhs_global(Edof(el,2:end)) = ...
            rhs_global(Edof(el,2:end)) + f_el;
        
        % For plotting:
        % Compute von Mises stress in the element:
        stresses_dev_el = stresses_el;
        stresses_dev_el(1:3) = stresses_el(1:3) - ...
            ones(3,1)*sum(stresses_el(1:3))/3.0;
        stress_Mises = sqrt(1.5* (stresses_dev_el'*stresses_dev_el) );
        
        stress_Mises_array(el) = stress_Mises;
    end % Loop over elements.
    
    gap_func_array = computeGapFunc(active_nodes_coords(:,2), ...
        u(active_dofs(:,2))+du(active_dofs(:,2)), y_top_r);
 
    if any(active_nodes)
        % Add contribution to the stiffness matrix and RHS vector from the
        % contact nodes:
        N_x = 0; % x component of the normal vector.
        N_y = 1; % y component of the normal vector.
       
        dGapFunc_dU_matrix = zeros(length(gap_func_array),...
            numel(active_dofs));
        for node=1:length(gap_func_array)
            dGapFunc_dU_matrix( node, 2*node-1:2*node) = [N_x,N_y];
        end
        
        K_global(active_dofs_reshaped, dof_extra) = dGapFunc_dU_matrix';
        K_global(dof_extra, active_dofs_reshaped) = dGapFunc_dU_matrix;
        K_global(dof_extra, dof_extra) = zeros(n_extra_dof);
        
        rhs_global(active_dofs_reshaped) = ...
            rhs_global(active_dofs_reshaped) + ...
            dGapFunc_dU_matrix' * Lagrange;
        rhs_global(dof_extra) = gap_func_array;
    end
    
    delta_dUnknowns =  - K_global(dof_free_total,dof_free_total) \ ...
        rhs_global(dof_free_total);
   
    du(dof_free_u) = du(dof_free_u) + delta_dUnknowns(1:n_dof_free_u);
    Lagrange = Lagrange + delta_dUnknowns(end-n_extra_dof+1:end);
    
    if norm(delta_dUnknowns) < err_tol
        converge = true;
        break
    end
end % Equilibrium iteration.

rhs_global_u = rhs_global(1:n_dof_u);

if converge
    % Report about completion:
    fprintf(['  Equilibrium was found in %d iterations with %d active ',...
        'nodes.\n'], iter, sum(active_nodes))
else
    error('Equilibrium was not found in %d iterations.\n', max_iter)
end

end % function findEquilibrium.

