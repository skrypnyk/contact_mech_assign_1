function [ stiffness, force, stresses ] = elementRoutine( x_array, ...
    y_array, mat_par, u, du)
% Assemble element stiffness matrix and element internal force vector.
%
% CALFEM (make sure to be installed) functions for triangular element
% are utilized.
%
%   Input:
% x_array -- x coordinates of the nodes of the element.
% y_array -- y coordinates of the nodes of the element.
% mat_par -- structure array with material parameters.
% u -- nodal displacements of the element.
% du -- increment of the element nodal displacements.
%
%   Output:
% stiffness -- element stiffness matrix.
% force -- element internal force vector.
% stresses -- stresses in the element.
%
%   Rostyslav Skrypnyk.

ptype = 2; % Plane strain.
t = 1; % Thickness.

[stresses, mat_stiff] = materialRoutine( x_array, y_array, mat_par, ...
    u, du, ptype, t);

% Assemble stiffness matrix and force vector:
[stiffness, ~] = plante(x_array, y_array, [ptype t], mat_stiff);
force = stiffness*(u+du);

end % function elementRoutine.

