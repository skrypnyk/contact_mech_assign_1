function [ gap_func_array ] = computeGapFunc(y_array, displacements, y_top)
%Returns a gap function array of the surface nodes that are in contact.
%   Computed gap function assumes that the master surface is flat.
%
%   Rostyslav Skrypnyk.

gap_func_array = y_array + displacements - y_top*ones(size(y_array));

end

