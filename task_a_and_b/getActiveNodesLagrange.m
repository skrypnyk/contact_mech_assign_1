function [ active_nodes ] = getActiveNodesLagrange( gap_func_array, ...
    tol, Lagrange)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% Regular check based on the geometry:
active_nodes = logical(zeros(size(gap_func_array)));
           
active_nodes(gap_func_array < tol) = true; % Mark nodes that are in contact.

% Additional check based on the sign of the Lagrange multipliers:
Lagrange_logical = logical(zeros(size(Lagrange)));

Lagrange_logical(Lagrange < 0) = true; % Lagrange multipliers are negative
% for real contact points.

active_nodes(active_nodes > 0) = ...
    active_nodes(active_nodes > 0) == Lagrange_logical;
end

