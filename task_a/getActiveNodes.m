function [ active_nodes ] = getActiveNodes( gap_func_array, tol )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

active_nodes = logical(zeros(size(gap_func_array)));
           
active_nodes(gap_func_array < tol) = true; % Mark nodes that are in contact.

end

