% A script for the assignment 1 on the computational part of Contact
% Mechanics course.
%% Assignment 1: One body.
% Based on CALFEM, solve contact problem between elastic body and rigid 
%plane under plane strain conditions with friction.
%
% Rostyslav Skrypnyk. Mar 9, 2016.

close all
clear variables
clc

addpath([pwd , '/../meshes'])
mesh = 'coarse';
%mesh = 'dense';

% Spatial discretization:
if strcmp(mesh,'coarse')
    mesh_r = load('calfem_rail_coarse.mat');
    mesh_w = load('calfem_wheel_coarse.mat'); % 1x1 structure array
    % with the following fields (with description from CALFEM manual):
    % Ex, Ey -- arrays of rows [x_1 x_2 ... x_nen] and [y_1 y_2 ... y_nen], 
    %           where nen = number of element nodes and row i gives x  
    %           (or y) coordinates of the element defined in row i of Edof. 
    % Coord -- global nodal coordinate matrix; array of [x y] rows.
    % Edof -- element topology matrix; array of [el dof_1 dof_2 ... dof_ned] 
    %         rows; el = element number, ned = number of element dof.
    % Dof -- global topology matrix; array of [k l .. m] rows. The nodal 
    %        coordinates defined in row i of Coord correspond to the 
    %        degrees  of freedom of row i in Dof.
    % dof_free
    % dof_prescr
    % contact_nodes.
elseif strcmp(mesh,'dense')
    mesh_r = load('calfem_rail_dense.mat');
    mesh_w = load('calfem_wheel_dense.mat');
else
    disp('Specify correct mesh density: coarse or dense.')
    return
end

% Boundary conditions:
% Fix x on the top of the wheel:
bc_w = [mesh_w.dof_prescr(1:2:end), ...
        zeros(length(mesh_w.dof_prescr(1:2:end)),1)]; 

% Loading:
u_vert = - 0.2; % Vertical displacement of the top of the wheel [mm].

% Time stepping:
n_steps = 10;
t_end = 1; % [s].
dt = t_end/n_steps;

% Material parameters:
mat_par.E = 200.e3; % Young's modulus [MPa=N/mm^2].
mat_par.nu = 0.3; % Poisson's ratio [unitless].
mat_par.penalty = 1.e7; % Penalty parameter [N/mm].

% Tolerances:
err_tol = 1.e-8; % Error tolerance for the Newton's iteration.
gap_tol = 1.e-12; % Tolerance for the getActiveNodes function.
max_iter = 20;

% Other quantities:
n_dof_w = max(max(mesh_w.Dof)); % Number of degrees of freedom.
n_dof_el_w = size(mesh_w.Edof,2) - 1; % Number of DOFs per element.
n_el_w = size(mesh_w.Edof,1); % Number of elements.
dof_free_w = mesh_w.dof_free;
dof_prescr_vert_w = mesh_w.dof_prescr(2:2:end);
y_top_r = max(mesh_r.Coord(:,2)); % Max ordinate in the rail geometry.
contact_dofs_w = mesh_w.Dof(mesh_w.contact_nodes,:); % DOFs of the nodes on 
% the contact surface.
contact_nodes_coords_w = mesh_w.Coord(mesh_w.contact_nodes,:);

% Initialize:
u_w = zeros(n_dof_w,1); % Displacements of the wheel at time t+dt.
du_w = zeros(n_dof_w,1);
du_prescr_w = u_vert/n_steps*ones(size(dof_prescr_vert_w)); % Increment of the 
% prescribed displacement.
u_hist_w = zeros(n_dof_w,n_steps);
forces_his_w = zeros(n_dof_w,n_steps);
stress_Mises_his_w = zeros(n_el_w,n_steps);
%% Task (a).
% Assume the rail is rigid while the wheel is elastic.
% Increase the verical displacement u_vert of the upper boundary of the 
% piece of the wheel while fixing the corresponding horizontal displacement
% u_hor = 0. Compute the total vertical reaction force F_vert vs the 
% displacement u_vert. Also compute the field of von Mises effective stress
% at a proper choice of u_vert. Use the penalty method. Show the dependence
% on the penalty parameter. Assume no friction.

for step = 1:n_steps
    % Compute gap function for the whole contact node set:
    gap_func_array_w = computeGapFunc(contact_nodes_coords_w(:,2), ...
        u_w(contact_dofs_w(:,2))+du_w(contact_dofs_w(:,2)), y_top_r);
    
    % Find which nodes from the contact set are active:
    active_nodes_w = getActiveNodes(gap_func_array_w, gap_tol);
    
    for iter=1:max_iter % Contact iteration:
        fprintf(' Start contact iteration %d.\n', iter)
        
        % Solve for equilibrium and update increment of the displacements:
        [du_w, forces_w, stress_Mises_array_w] = findEquilibrium(mesh_w.Edof, mesh_w.Ex, ...
            mesh_w.Ey, mat_par, u_w, du_w, du_prescr_w, n_el_w, ...
            n_dof_w, dof_free_w, dof_prescr_vert_w, ...
            contact_nodes_coords_w, contact_dofs_w, active_nodes_w, y_top_r, max_iter, err_tol);

        % Recompute gap function:
        gap_func_array_w = computeGapFunc(contact_nodes_coords_w(:,2), ...
             u_w(contact_dofs_w(:,2))+du_w(contact_dofs_w(:,2)), y_top_r);
        
        % Find the active nodes after the equilibrium has been found:
        active_nodes_w_new = getActiveNodes(gap_func_array_w, gap_tol);
        
        if all(active_nodes_w==active_nodes_w_new)
            % If the set of active contact nodes has not changed:
            break
        end
        active_nodes_w = active_nodes_w_new;
    end % Contact iteration.
    if iter==max_iter
        error('Contact iteration did not converge in %d iterations.\n', ...
            max_iter)
    else
        fprintf('Competed step %d out of %d steps.\n\n', step, n_steps)
    end
    u_w = u_w + du_w;
    u_hist_w(:,step) = u_w;
    forces_his_w(:,step) = forces_w;
    stress_Mises_his_w(:,step) = stress_Mises_array_w;
end % Loop over time.

figure(1)
% Plot undeformed shape:
eldraw2(mesh_w.Ex, mesh_w.Ey,[2 1 0]) % Black.
%eldraw2(mesh_r.Ex, mesh_r.Ey,[1 4 0]) % Red.

% Plot deformed shape:
u_el_array = extract(mesh_w.Edof, u_w);
eldisp2(mesh_w.Ex,mesh_w.Ey,u_el_array,[1,2,0],1);
xlim([-36,36])
ylim([0,41])

figure(2)
% Plot the total vertical reaction force vs the displacement:
plot(u_hist_w(dof_prescr_vert_w(1),:)/u_vert, ...
    sum(forces_his_w(dof_prescr_vert_w,:),1))
% hold on
% plot(u_hist_w(dof_prescr_vert_w(1),[1,5,10])/u_vert,sum(forces_his_w(dof_prescr_vert_w,[1,5,10]),1))
xlabel('Normalized vertical displacement, [unitless]', 'interpreter','latex','fontsize', 18)
ylabel('Total vertical reaction force, [N]', 'interpreter','latex','fontsize',18)
h = gca;
h.FontSize = 14;
% print -depsc ./doc/img/force_displ % Create file for LaTeX.

% figure(3)
% % Plot von Mises stress:
% fill(mesh_w.Ex',mesh_w.Ey',stress_Mises_his_w(:,end)'); 
% colorbar; 
% axis equal;
% title('von Mises stress, [MPa]','interpreter','latex','fontsize',18)
% xlabel('$x$, [mm]', 'interpreter','latex','fontsize',18)
% ylabel('$y$, [mm]', 'interpreter','latex','fontsize', 18)
% h = gca;
% h.FontSize = 14;
% % print -depsc ./doc/img/von_Mises % Create file for LaTeX.