% Plots for task B.
% /Rostyslav Skrypnyk.

figure(2)
load('lagrangecoarse.mat')
plot(u_hist_w(dof_prescr_vert_w(1),:)/u_vert, ...
    sum(forces_hist_w(dof_prescr_vert_w,:),1),'*-')
hold on

load('lagrange.mat')
plot(u_hist_w(dof_prescr_vert_w(1),:)/u_vert, ...
    sum(forces_hist_w(dof_prescr_vert_w,:),1),'r^-')

load('penalty1e7.mat')
plot(u_hist_w(dof_prescr_vert_w(1),:)/u_vert, ...
    sum(forces_hist_w(dof_prescr_vert_w,:),1),'ks-')
hold off

xlabel('Normalized vertical displacement, [unitless]', 'interpreter','latex','fontsize', 18)
ylabel('Total vertical reaction force, [N]', 'interpreter','latex','fontsize',18)
legend({'Lagrange method, coarse mesh', 'Lagrange method, fine mesh', ...
        'Penalty method, fine mesh, $\epsilon_N = 1e7$ N/mm'},'interpreter','latex',...
        'location', 'best')
h = gca;
h.FontSize = 14;
set(h,'TickLabelInterpreter', 'latex');
% print -depsc ./doc/img/force_displ_taskb % Create file for LaTeX.