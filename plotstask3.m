% Plots for task C.
% /Rostyslav Skrypnyk.

figure(3)
% Plot tangential velocities:
[coords_sorted, s_i] = sort(contact_nodes_coords_w(:,1));
plot(coords_sorted,dv_tang_contact_w(s_i,1)/(u_horiz/n_steps_horiz/dt_old),'*-')

xlabel('$x$, [mm]', 'interpreter','latex','fontsize',18)
ylabel('Tangential velocity, [mm/s]', 'interpreter','latex','fontsize', 18)
h = gca;
h.FontSize = 14;
set(h,'TickLabelInterpreter', 'latex');
c.TickLabelInterpreter = 'latex';
% print -depsc ./doc/img/velocity % Create file for LaTeX.