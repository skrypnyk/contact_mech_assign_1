function [ stresses, mat_stiff ] = materialRoutine( x_array, y_array, ...
    mat_par, u, du, ptype, t )
%Returns stresses and material stiffness.
%
% CALFEM (make sure to be installed) functions for triangular element
% are utilized.
%
%   Input:
% x_array -- x coordinates of the nodes of the element.
% y_array -- y coordinates of the nodes of the element.
% mat_par -- structure array with material parameters.
% u -- nodal displacements of the element.
% du -- increment of the element nodal displacements.
% ptype -- type of analysis (plane stress or strain, see CALFEM manual).
% t -- thickness.
%
%   Output:
% stresses -- stresses in the element.
% mat_stiffness -- material stiffness.
%
%   Rostyslav Skrypnyk.

mat_stiff = hooke(ptype, mat_par.E, mat_par.nu);

[stresses,~] = plants(x_array, y_array, [ptype t], mat_stiff, (u+du)');

stresses = stresses';

end % function materialRoutine.

